import http from './http'

async function getList(data) {
    let res = await http({
        url:'http://localhost:8080/api/recommend_api/v1/article/recommend_all_feed?aid=2608&uuid=7129106312282097155',
        method:'POST',
        data
    })
    return res.data
}
 
export  {
    getList
}
