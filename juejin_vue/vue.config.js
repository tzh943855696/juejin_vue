module.exports = {
    devServer: {
        proxy: {
            "/api": {
                target: 'https://api.juejin.cn', //目标接口域名
                secure: true,            //false为http访问，true为https访问
                changeOrigin: true,      //是否跨域
                //重写路径  
                pathRewrite: {
                    '^/api': ''
                },
                headers: {
                    origin: 'https://api.juejin.cn'
                }
            },
        },
    }
}